#ifndef __SIMPLEX_GRAPHICS_GRAPHICS_API_ADAPTEE_H__
#define __SIMPLEX_GRAPHICS_GRAPHICS_API_ADAPTEE_H__

#include <Simplex/Core/Adaptee.h>
#include <Simplex/Graphics/Shader.h>
#include <vector>

namespace Simplex
{
  namespace Graphics
  {
  	class Renderable;
  	class Mesh;

    class GraphicsApiAdaptee : public Simplex::Core::Adaptee
    {
    public:
      virtual void Initialize() = 0;
      virtual void Shutdown() = 0;

      /* 
      =========================================
        Rendering
      =========================================
      */
      // When implementing the adaptee, you'll have to store a reference to this renderable's
      // data tied to the integer you return here. 
      // This integer will be the index used to call all future methods associated to the renderable.
      virtual void AddRenderable ( Simplex::Graphics::Renderable* renderable ) = 0;
      virtual void LoadRenderable ( int index ) = 0;
      virtual void Render ( int index ) = 0;

      /* 
      =========================================
        Shaders
      =========================================
      */
      virtual void AddShader ( Simplex::Graphics::Shader* shader ) = 0;
      virtual void LoadShader ( int index ) = 0;
      virtual void UseShader ( int index ) = 0;
      virtual void SetShaderParameter ( std::string name, void* values, int count ) = 0;

    };
  }
}

#endif