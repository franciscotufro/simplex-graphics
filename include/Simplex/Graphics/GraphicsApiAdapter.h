#ifndef __SIMPLEX_CORE_GRAPHICS_API_ADAPTER_H__
#define __SIMPLEX_CORE_GRAPHICS_API_ADAPTER_H__

#include <Simplex/Graphics/GraphicsApiAdaptee.h>
#include <Simplex/Core/Adapter.h>
#include <Simplex/Core/Exception.h>
#include <vector>

namespace Simplex
{
  namespace Graphics
  {
    class Renderable;
    class Mesh;

    typedef Simplex::Core::Exception NullGraphicsApiAdapter;

    class GraphicsApiAdapter : public Simplex::Core::Adapter
    {
      
    public:
      static GraphicsApiAdapter* Instance ();

      void Initialize ();
      
      void Shutdown ();

      GraphicsApiAdaptee* Adaptee();

      void AddRenderable ( Simplex::Graphics::Renderable* renderable );
      
      void LoadRenderable ( int index );

      void Render ( int index );
      
      void AddShader ( Simplex::Graphics::Shader* shader );
      // void LoadShader ( int index ) = 0;
      // void UseShader ( int index ) = 0;
      // void SetShaderParameter ( std::string name, void* values, int count ) = 0;

    private:
      static GraphicsApiAdapter* mAdapter;
      static GraphicsApiAdapter* GetExistingAdapterOrCreateOne ();

      void EnsureAdapteeIsSet ();
      
    };
  }
}

#endif