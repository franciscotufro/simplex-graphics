#ifndef __SIMPLEX_GRAPHICS_GRAPHICS_API_INDEXABLE_H__
#define __SIMPLEX_GRAPHICS_GRAPHICS_API_INDEXABLE_H__


namespace Simplex
{
  namespace Graphics
  {
    class GraphicsApiIndexable
    {

    public:
      unsigned int GetGraphicsApiIndex ();
      void SetGraphicsApiIndex ( unsigned int index );

    private:
      unsigned int mGraphicsApiIndex;
    };
  }
}

#endif