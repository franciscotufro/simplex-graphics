#ifndef __SIMPLEX_GRAPHICS_MATERIAL_H__
#define __SIMPLEX_GRAPHICS_MATERIAL_H__

#include <Simplex/Graphics/Shader.h>

namespace Simplex
{
  namespace Graphics
  {
    class Material
    {
    public:
      Shader* GetShader ( ShaderType type );
      void AttachShader ( Shader* shader );
      
    private:
      Shader* mShaders[ShaderType::COUNT];
    };
  }
}

#endif