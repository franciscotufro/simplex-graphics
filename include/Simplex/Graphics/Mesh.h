#ifndef __SIMPLEX_GRAPHICS_MESH_H__
#define __SIMPLEX_GRAPHICS_MESH_H__

#include <Simplex/Math/Vector3.h>

namespace Simplex
{
  namespace Graphics
  {
    class Mesh
    {
      
    public:
      Mesh ( int mVertexCount, Simplex::Math::Vector3* mVertices );
      ~Mesh ();

      int GetVertexCount ();
      Simplex::Math::Vector3& GetVertexAt ( int i );

    private:
      Simplex::Math::Vector3* mVertices;
      int mVertexCount;
    };
  }
}

#endif