#ifndef __SIMPLEX_GRAPHICS_RENDERABLE_H__
#define __SIMPLEX_GRAPHICS_RENDERABLE_H__

#include <Simplex/Core/SceneObject.h>
#include <Simplex/Graphics/Mesh.h>
#include <Simplex/Graphics/Shader.h>
#include <Simplex/Graphics/Material.h>
#include <Simplex/Graphics/GraphicsApiIndexable.h>

namespace Simplex
{
  namespace Graphics
  {
    class Renderable : public Simplex::Core::SceneObject, public Simplex::Graphics::GraphicsApiIndexable
    {
      
    public:
      Renderable();
      ~Renderable();
      
      virtual void StartCallback ();
      virtual void RenderCallback ( Simplex::Math::Matrix4* currentTransform );
      virtual void UpdateCallback ();
      
      void RenderSetup ();
      void RenderTeardown ();

      // When creating a custom Renderable, you'll need to implement
      // the Start, Render and Update methods for customization.
      virtual void Start ( ) {};
      virtual void Render (  Simplex::Math::Matrix4* const currentTransform ) {};
      virtual void Update ( ) {};
      
      Simplex::Graphics::Mesh* GetMesh ();
      void SetMesh ( Simplex::Graphics::Mesh* mesh );

      Simplex::Graphics::Shader::Info& GetShaderAt ( int index );
      void AddShader ( Simplex::Graphics::ShaderType type, const char * filename );

      Simplex::Graphics::Material* GetMaterial ( );
      void SetMaterial ( Simplex::Graphics::Material* material );
      
    private:
      Mesh* mMesh;
      Material* mMaterial;
      
      std::vector<Shader::Info> mShaders;
      int mGraphicsApiIndex;

    };
  }
}

#endif