#ifndef __SIMPLEX_GRAPHICS_SHADER_H__
#define __SIMPLEX_GRAPHICS_SHADER_H__

#include <Simplex/Core/Exception.h>
#include <Simplex/Core/Asset.h>

namespace Simplex
{
  namespace Graphics
  {
    enum ShaderType { NONE, VERTEX, TESSELLATION_CONTROL, TESSELLATION_EVALUATION, GEOMETRY, FRAGMENT, COMPUTE, COUNT };

    struct ShaderDefinition
    {
        std::string name;
        std::string filename;
        ShaderType type;
        unsigned int id;
    };

    class Shader : public Simplex::Core::Asset<ShaderDefinition>
    {
    public:
      typedef Simplex::Core::Exception ShaderTypeNotSet;
      
      

      typedef struct {
        ShaderType type;
        const char* filename;
        unsigned int shader;
      } Info;

      Shader ();
      ~Shader ();
      
      ShaderType GetType ();
      void SetType ( ShaderType shaderType );

      std::string GetFilename ();
      void SetFilename ( std::string filename );

      void ParseDefinition ();
      
      void Load ();

    private:
      ShaderType mType;
      std::string mFilename;
      
    };


  }
}

#endif