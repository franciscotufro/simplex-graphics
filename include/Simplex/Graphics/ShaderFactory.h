#ifndef __SIMPLEX_GRAPHICS_SHADER_FACTORY_H__
#define __SIMPLEX_GRAPHICS_SHADER_FACTORY_H__

#include <Simplex/Core/AssetFactory.h>
#include <Simplex/Graphics/Shader.h>
#include <string>

namespace Simplex
{
  namespace Graphics
  {
    class ShaderFactory : public Simplex::Core::AssetFactory<Shader, ShaderDefinition>
    {
    public:
        static ShaderFactory* Instance ();

    private:
        
        static ShaderFactory* mShaderFactory;

        static ShaderFactory* GetExistingShaderFactoryOrCreateOne ();
        ShaderFactory();

    };
  }
}

#endif