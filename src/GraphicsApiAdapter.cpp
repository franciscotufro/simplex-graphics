#include <Simplex/Graphics/GraphicsApiAdapter.h>
#include <iostream>
#include <sstream>

using namespace Simplex::Core;
namespace Simplex
{

  namespace Graphics
  {
    GraphicsApiAdapter* GraphicsApiAdapter::mAdapter = NULL;

    GraphicsApiAdapter* GraphicsApiAdapter::Instance()
    {
      return GetExistingAdapterOrCreateOne ();
    }

    GraphicsApiAdapter* GraphicsApiAdapter::GetExistingAdapterOrCreateOne () 
    {
      if (!mAdapter)
        mAdapter = new GraphicsApiAdapter ();

      return mAdapter;
    }

    void GraphicsApiAdapter::Initialize ()
    {
      EnsureAdapteeIsSet();
      Adaptee()->Initialize();
    }

    void GraphicsApiAdapter::Shutdown ()
    {
      EnsureAdapteeIsSet();
      Adaptee()->Shutdown();
    }

    void GraphicsApiAdapter::AddRenderable ( Simplex::Graphics::Renderable* renderable )
    {
      EnsureAdapteeIsSet();
      Adaptee()->AddRenderable ( renderable ); 
    }

    void GraphicsApiAdapter::LoadRenderable ( int index )
    {
      EnsureAdapteeIsSet();
      Adaptee()->LoadRenderable ( index );
    }

    void GraphicsApiAdapter::Render ( int index )
    {
      Adaptee()->Render ( index );
    }

    void GraphicsApiAdapter::AddShader ( Simplex::Graphics::Shader* shader )
    {
      EnsureAdapteeIsSet();
      Adaptee()->AddShader ( shader ); 
    }



    GraphicsApiAdaptee* GraphicsApiAdapter::Adaptee()
    {
      return ((GraphicsApiAdaptee*)GetAdaptee());
    }

    void GraphicsApiAdapter::EnsureAdapteeIsSet ()
    {
      NullGraphicsApiAdapter e = Exception("Graphics API Adaptee is NULL");
      Exception::NullCheck ( Adaptee(), e );
    }
    
  }
}