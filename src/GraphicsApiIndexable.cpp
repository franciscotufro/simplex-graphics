#include <Simplex/Graphics/GraphicsApiIndexable.h>

namespace Simplex
{
	namespace Graphics
	{
    unsigned int GraphicsApiIndexable::GetGraphicsApiIndex ()
    {
      return mGraphicsApiIndex;
    }

    void GraphicsApiIndexable::SetGraphicsApiIndex ( unsigned int index )
    {
      mGraphicsApiIndex = index;
    }
	}
}
