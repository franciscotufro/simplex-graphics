#include <Simplex/Graphics/Material.h>

namespace Simplex
{

  namespace Graphics
  {
      Shader* Material::GetShader ( ShaderType type)
      {
        return mShaders[type];
      }
      
      void Material::AttachShader ( Shader* shader)
      {
        mShaders[shader->GetType()] = shader;
     }
      
 }
}