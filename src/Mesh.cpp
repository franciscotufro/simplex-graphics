#include <Simplex/Graphics/Mesh.h>

using namespace Simplex::Math;

namespace Simplex
{

  namespace Graphics
  {
      Mesh::Mesh ( int vertexCount, Simplex::Math::Vector3* vertices )
      {
        mVertexCount = vertexCount;

        mVertices = new Vector3[mVertexCount];

        for(int i = 0; i < mVertexCount; i++)
        {
          mVertices[i] = Vector3( vertices[i] );
        }

      }
      
      Mesh::~Mesh ()
      {}

      //int Mesh::VectorCount ();
      Simplex::Math::Vector3& Mesh::GetVertexAt (int i)
      {
        return mVertices[i];
      }
    
      int Mesh::GetVertexCount ()
      {
        return mVertexCount;
      }
  }
}