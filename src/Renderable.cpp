#include <Simplex/Graphics/Renderable.h>
#include <Simplex/Graphics/GraphicsApiAdapter.h>
using namespace Simplex::Math;
using namespace Simplex::Core;
namespace Simplex
{

  namespace Graphics
  {
    Renderable::Renderable ()
    {
    }
    
    Renderable::~Renderable ()
    {
    }

    void Renderable::StartCallback ( )
    {
      // // TODO: Test all this stuff
      GraphicsApiAdapter* adapter = GraphicsApiAdapter::Instance();
      adapter->AddRenderable ( this );
      adapter->LoadRenderable ( GetGraphicsApiIndex () );
      Start ();
    }

    void Renderable::RenderCallback ( Simplex::Math::Matrix4* const currentTransform )
    {
      RenderSetup ();
      Render ( currentTransform );
      RenderTeardown ();
    }
    
    void Renderable::RenderSetup ()
    {
      GraphicsApiAdapter* adapter = GraphicsApiAdapter::Instance();
      adapter->Render ( GetGraphicsApiIndex() );
    }
    
    void Renderable::RenderTeardown ()
    {
      
    }
 
    void Renderable::UpdateCallback ()
    {
      Update ();
    }
    
    Simplex::Graphics::Mesh* Renderable::GetMesh ()
    {
      return mMesh;
    }

    void Renderable::SetMesh ( Simplex::Graphics::Mesh* mesh )
    {
      mMesh = mesh;
    }

    Simplex::Graphics::Material* Renderable::GetMaterial ()
    {
      return mMaterial;
    }

    void Renderable::SetMaterial ( Simplex::Graphics::Material* material )
    {
      mMaterial = material;
    }

    Simplex::Graphics::Shader::Info& Renderable::GetShaderAt (int index)
    {
      return mShaders[index];
    }

    void Renderable::AddShader ( Simplex::Graphics::ShaderType type, const char * filename )
    {
      Simplex::Graphics::Shader::Info info = { type, filename };
      mShaders.push_back(info);
    }

  }
  
}