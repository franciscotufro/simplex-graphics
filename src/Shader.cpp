#include <Simplex/Graphics/Shader.h>
#include <Simplex/Graphics/GraphicsApiAdapter.h>

using namespace Simplex::Core;

namespace Simplex
{

  namespace Graphics
  {
      Shader::Shader ()
      {
        mType = NONE;
      }
      
      Shader::~Shader ()
      {

      }
      
      ShaderType Shader::GetType ()
      {
        return mType;
      }
      
      void Shader::SetType ( ShaderType type )
      {
        mType = type;
      }

      std::string Shader::GetFilename ()
      {
        return mFilename;
      }
      
      void Shader::SetFilename ( std::string filename )
      {
        mFilename = filename;
      }


      void Shader::ParseDefinition ()
      {
        SetType ( GetDefinition().type );
        SetFilename ( GetDefinition().filename );
      }

      void Shader::Load ()
      {
        GraphicsApiAdapter::Instance()->LoadShader ( this );
      }
  }
}