#include <Simplex/Graphics/ShaderFactory.h>

namespace Simplex
{
  namespace Graphics
  {

    ShaderFactory* ShaderFactory::mShaderFactory = NULL;

    ShaderFactory::ShaderFactory()
    {
    }

    ShaderFactory* ShaderFactory::Instance()
    {
      return GetExistingShaderFactoryOrCreateOne ();
    }

    ShaderFactory* ShaderFactory::GetExistingShaderFactoryOrCreateOne () 
    {
      if (!mShaderFactory)
        mShaderFactory = new ShaderFactory ();

      return mShaderFactory;
    }

  }
}