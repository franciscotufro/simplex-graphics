#ifndef __GRAPHICS_API_ADAPTEE_MOCK_HELPER__
#define __GRAPHICS_API_ADAPTEE_MOCK_HELPER__

#include <Simplex/Graphics/GraphicsApiAdaptee.h>
#include <Simplex/Graphics/Renderable.h>
#include <Simplex/Graphics/Mesh.h>

class GraphicsApiAdapteeMock : public Simplex::Graphics::GraphicsApiAdaptee
{
public:
  bool Initialized;
  bool ShuttedDown;
  bool RenderCalledWith1337AsParameter;
  bool LoadRenderableCalledWith1337AsParameter;
  bool LoadRenderableCalledWith3AsParameter;
  bool AddShaderCalled;
  int loadedIndex;
  
  Simplex::Graphics::Mesh* loadedMesh;
  std::vector<Simplex::Graphics::Shader::Info> loadedShaderInfo;

  GraphicsApiAdapteeMock() 
  {
    Initialized = false;
    ShuttedDown = false;
    RenderCalledWith1337AsParameter = false;
    LoadRenderableCalledWith1337AsParameter = false;
    LoadRenderableCalledWith3AsParameter = false;
    AddShaderCalled = false;
  }
  
  void Initialize()
  {
    Initialized = true;
  }

  void Shutdown()
  {
    ShuttedDown = true;
  }

  void AddRenderable ( Simplex::Graphics::Renderable* renderable )
  {
    renderable->SetGraphicsApiIndex ( 3 );
  }
  
  Simplex::Graphics::Mesh& GetLoadedMesh ( int index )
  {
    if(index == loadedIndex)
    {
      return *loadedMesh;
    }
  }

  void LoadRenderable ( int index )
  {
    LoadRenderableCalledWith1337AsParameter = ( index == 1337 );
    LoadRenderableCalledWith3AsParameter = ( index == 3 );
  }

  std::vector<Simplex::Graphics::Shader::Info>& GetLoadedShaders ( int index )
  {
    if( index == loadedIndex )
    {
      return loadedShaderInfo;  
    }
  }

  void Render ( int index )
  {
    RenderCalledWith1337AsParameter = ( index == 1337 );
  }


  void AddShader ( Simplex::Graphics::Shader* shader )
  {
    AddShaderCalled = true;
  };

  void LoadShader ( int index ) {}
  void UseShader ( int index ) {}
  void SetShaderParameter ( std::string name, void* values, int count ) {}

};

#endif