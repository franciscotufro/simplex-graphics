#include <Simplex/Test.h>
#include <Simplex/Graphics/GraphicsApiAdapter.h>
#include <Simplex/Core/Exception.h>
#include "GraphicsApiAdapteeMock.h"
#include "RenderableMock.h"
#include "MeshMock.h"

using namespace Simplex::Graphics;
using namespace Simplex::Core;

struct GraphicsApiAdapterTests {
  GraphicsApiAdapter* adapter;
  GraphicsApiAdapteeMock* adaptee;
} objects;

void InitializeGraphicsApiAdapterIfNotInitialized ()
{
  objects.adapter = GraphicsApiAdapter::Instance ();
  if ( !objects.adapter->HasAdaptee() )
  {
    objects.adaptee = new GraphicsApiAdapteeMock();
    objects.adapter->SetAdaptee( objects.adaptee );
  }

}

TEST ( SimplexGraphicsGraphicsApiAdapter, InitializeCallsInitializeOnAdaptee )
{
  InitializeGraphicsApiAdapterIfNotInitialized();
  
  objects.adapter->Initialize();
  
  ASSERT_TRUE ( objects.adaptee->Initialized );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, ShutdownCallsShutdownOnAdaptee )
{
  InitializeGraphicsApiAdapterIfNotInitialized();

  objects.adapter->Shutdown();
  
  ASSERT_TRUE ( objects.adaptee->ShuttedDown );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, IsASingleton )
{
  ASSERT_TRUE ( GraphicsApiAdapter::Instance () );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, AddRenderableReturnsAnIndex )
{
  InitializeGraphicsApiAdapterIfNotInitialized ();
  RenderableMock* r;
  r->SetGraphicsApiIndex ( 1337 );

  objects.adapter->AddRenderable ( r );

  ASSERT_NE ( 1337, r->GetGraphicsApiIndex() );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, AddRenderableThrowsExceptionIfNoAdapteeIsSet )
{
  objects.adapter->SetAdaptee(0);
  RenderableMock* r;

  try
  {
    objects.adapter->AddRenderable ( r );
  }
  catch ( Exception& e )
  {
    ASSERT_EQ( "Graphics API Adaptee is NULL", e.GetDescription() );
  }
}

TEST ( SimplexGraphicsGraphicsApiAdapter, LoadRenderableCallsLoadRenderableOnAdapteeWithCorrectParams )
{
  InitializeGraphicsApiAdapterIfNotInitialized();
  
  objects.adapter->LoadRenderable(1337);
  
  ASSERT_TRUE ( objects.adaptee->LoadRenderableCalledWith1337AsParameter );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, RenderCallsRenderOnAdapteeWithCorrectParams )
{
  InitializeGraphicsApiAdapterIfNotInitialized();
  
  objects.adapter->Render(1337);
  
  ASSERT_TRUE ( objects.adaptee->RenderCalledWith1337AsParameter );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, AddShaderCallsAddShaderInAdaptee )
{
  InitializeGraphicsApiAdapterIfNotInitialized();

  Shader* shader = new Shader ();
  
  objects.adapter->AddShader( shader );

  ASSERT_TRUE ( objects.adaptee->AddShaderCalled );

  delete shader;

}

TEST ( SimplexGraphicsGraphicsApiAdapter, RenderCallsRenderInAdaptee )
{
  InitializeGraphicsApiAdapterIfNotInitialized ();
  
  objects.adapter->Render ( 1337 );

  ASSERT_TRUE ( objects.adaptee->RenderCalledWith1337AsParameter );
}

TEST ( SimplexGraphicsGraphicsApiAdapter, AdapteeReturnsCurrentAdaptee )
{
  InitializeGraphicsApiAdapterIfNotInitialized ();
  
  Simplex::Graphics::GraphicsApiAdaptee* adaptee = objects.adapter->Adaptee();

  ASSERT_EQ ( objects.adaptee, adaptee  );
}