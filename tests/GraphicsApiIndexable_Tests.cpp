#include <Simplex/Test.h>
#include <Simplex/Graphics/GraphicsApiIndexable.h>
#include "RenderableMock.h"

TEST ( SimplexGraphicsApiIndexable, SetGraphicsApiIndexWorks )
{
  Simplex::Graphics::GraphicsApiIndexable* indexable = new Simplex::Graphics::GraphicsApiIndexable ();

  indexable->SetGraphicsApiIndex ( 1337 );

  ASSERT_EQ ( 1337, indexable->GetGraphicsApiIndex() );
}
