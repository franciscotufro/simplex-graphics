#include <Simplex/Test.h>
#include <Simplex/Graphics/Material.h>
#include <Simplex/Graphics/Shader.h>

using namespace Simplex::Graphics;

TEST ( SimplexGraphicsMaterial, CanAttachShadersOfAllTypes )
{
  Material material;

  ShaderType types[] = {
    ShaderType::NONE,
    ShaderType::VERTEX,
    ShaderType::TESSELLATION_CONTROL,
    ShaderType::TESSELLATION_EVALUATION,
    ShaderType::GEOMETRY,
    ShaderType::FRAGMENT,
    ShaderType::COMPUTE,
  };
  
  for( int i = 0; i < ShaderType::COUNT; i++)
  {
    Shader shader;
    shader.SetType ( types[i] );

    material.AttachShader ( &shader );

    ASSERT_EQ ( &shader, material.GetShader ( types[i] ) );
  }
}