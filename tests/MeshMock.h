#ifndef __MESH_MOCK_H__
#define __MESH_MOCK_H__

#include <Simplex/Graphics/Mesh.h>

using namespace Simplex::Graphics;
using namespace Simplex::Math;

Mesh* MeshMock ()
{
	Vector3 vertices[3] = 
	{
		Vector3(0, 0, 0),
		Vector3(0, 1, 0),
		Vector3(1, 1, 1)
	};

	Mesh* mesh = new Mesh(3, vertices);
	return mesh;
}

#endif