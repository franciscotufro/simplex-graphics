#include <Simplex/Test.h>
#include <Simplex/Graphics/Mesh.h>

using namespace Simplex::Graphics;
using namespace Simplex::Math;

Vector3 vertices[3] = 
{
	Vector3(0, 0, 0),
	Vector3(0, 1, 0),
	Vector3(1, 1, 1)
};



TEST ( SimplexGraphicsMesh, CreatesAMeshWithVector3 )
{
	Mesh mesh = Mesh(3, vertices);

	ASSERT_EQ(vertices[0], mesh.GetVertexAt(0) );
}

TEST ( SimplexGraphicsMesh, GetVertexCountReturnsVertexCount )
{
	Mesh mesh = Mesh(3, vertices);

	ASSERT_EQ(3, mesh.GetVertexCount() );
}