#ifndef __RENDERABLE_MOCK_H__
#define __RENDERABLE_MOCK_H__

#include <Simplex/Graphics/Renderable.h>

class RenderableMock : public Simplex::Graphics::Renderable
{
public:
	bool startCalled;
	bool updateCalled;
	bool renderCalled;

	RenderableMock ()
	{
		startCalled = false;
		updateCalled = false;
		renderCalled = false;
	}

	void Start ()
	{
		startCalled = true;
	}

  void Render (  Simplex::Math::Matrix4* const currentTransform ) 
  {
  	renderCalled = true;
  }

  void Update ()
  {
  	updateCalled = true;
  }

};

#endif