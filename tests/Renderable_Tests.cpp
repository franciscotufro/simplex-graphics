#include <Simplex/Test.h>
#include <Simplex/Core/SceneGraph.h>
#include <Simplex/Graphics/Shader.h>
#include <Simplex/Graphics/Material.h>
#include <Simplex/Graphics/GraphicsApiAdapter.h>
#include "RenderableMock.h"
#include "GraphicsApiAdapteeMock.h"

using namespace Simplex::Core;
using namespace Simplex::Graphics;

void SetMeshTo ( Renderable* renderable )
{
  Simplex::Math::Vector3 vertices[3] = { Vector3(1, 1, 1), Vector3(0, 1, 0), Vector3(1, 0, 1) };
  Mesh* mesh = new Mesh(3, vertices);
  renderable->SetMesh ( mesh );
}

TEST ( SimplexGraphicsRenderable, InheritsFromSimplexGraphicsGraphicsApiIndexable )
{
  Renderable renderable = Renderable ();
  
  ASSERT_TRUE( static_cast< Simplex::Graphics::GraphicsApiIndexable* >( &renderable ) );
}

TEST ( SimplexGraphicsRenderable, InheritsFromSimplexCoreSceneObject )
{
  Renderable renderable = Renderable ();
  
  ASSERT_TRUE( static_cast< Simplex::Core::SceneObject* >( &renderable ) );
}

TEST ( SimplexGraphicsRenderable, AddToSceneCallsStart )
{
  RenderableMock* renderable = new RenderableMock ();
  SceneGraph s;
  s.Start ();

  s.AddSceneObject ( renderable );

  ASSERT_TRUE ( renderable->startCalled );
  delete renderable;
}

TEST ( SimplexGraphicsRenderable, SceneRenderCallbackCallsRender )
{
  RenderableMock* renderable = new RenderableMock ();
  SceneGraph s;

  s.AddSceneObject ( renderable );
  s.Render ();
  
  ASSERT_TRUE ( renderable->renderCalled );
  delete renderable;

}

TEST ( SimplexGraphicsRenderable, SceneUpdateCallbackCallsUpdate )
{
  RenderableMock* renderable = new RenderableMock ();
  SceneGraph s;

  s.AddSceneObject ( renderable );
  s.Update ();
  
  ASSERT_TRUE ( renderable->updateCalled );
  delete renderable;
}

TEST ( SimplexGraphicsRenderable, SetMeshWorks )
{
  RenderableMock* renderable = new RenderableMock ();
  SetMeshTo ( renderable );
  Mesh* mesh = renderable->GetMesh ();

  ASSERT_EQ ( mesh->GetVertexCount(), renderable->GetMesh()->GetVertexCount() );
  for(int i = 0; i < 3; i++)
  {
    ASSERT_EQ ( mesh->GetVertexAt ( i ), renderable->GetMesh()->GetVertexAt ( i ) );
  }

  delete mesh;
  delete renderable;
}

TEST ( SimplexGraphicsRenderable, AddShaderAddsShaderToShaderList )
{
  RenderableMock* renderable = new RenderableMock ();

  renderable->AddShader ( ShaderType::VERTEX, "./Shaders/triangles.vert" );

  ASSERT_EQ ( ShaderType::VERTEX, renderable->GetShaderAt(0).type );
  ASSERT_EQ ( "./Shaders/triangles.vert", renderable->GetShaderAt(0).filename );
}

TEST ( SimplexGraphicsRenderable, SetMaterialWorks )
{
  RenderableMock* renderable = new RenderableMock ();
  
  Material material;
  
  renderable->SetMaterial ( &material );

  ASSERT_EQ ( &material, renderable->GetMaterial () );
}

TEST ( SimplexGraphicsRenderable, StartCallbackAddsRenderableToGraphicsApi )
{
  RenderableMock* renderable = new RenderableMock ();
  SetMeshTo ( renderable );
  renderable->SetGraphicsApiIndex ( 1337 );

  renderable->StartCallback();

  ASSERT_NE ( 1337, renderable->GetGraphicsApiIndex() );
}

TEST ( SimplexGraphicsRenderable, StartCallbackLoadsRenderable )
{
  RenderableMock* renderable = new RenderableMock ();
  SetMeshTo ( renderable );
  Simplex::Graphics::GraphicsApiAdapter* adapter = Simplex::Graphics::GraphicsApiAdapter::Instance ();
  adapter->LoadRenderable( 100 );

  renderable->StartCallback();

  ASSERT_TRUE ( ((GraphicsApiAdapteeMock*) adapter->Adaptee ())->LoadRenderableCalledWith3AsParameter );
}