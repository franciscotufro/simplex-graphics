#include <Simplex/Test.h>
#include <Simplex/Graphics/ShaderFactory.h>

using namespace Simplex::Graphics;

typedef Simplex::Core::AssetFactory<Shader, ShaderDefinition> shaderFactory;

TEST ( SimplexGraphicsShaderFactory, InheritsFromSimplexCoreAssetFactory )
{
  ASSERT_TRUE( static_cast< shaderFactory* >(ShaderFactory::Instance() ) );
}

TEST ( SimplexGraphicsShaderFactory, CanCreateAShader )
{
	ShaderDefinition shaderDefinition = { "MyShader", "my_shader.frag", ShaderType::VERTEX };
	
	ShaderFactory::Instance()->Define ( shaderDefinition );

	Shader* shader = ShaderFactory::Instance()->Create ( "MyShader" );

	ASSERT_EQ ( ShaderType::VERTEX, shader->GetType());

}
