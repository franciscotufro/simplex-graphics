#include <Simplex/Test.h>
#include <Simplex/Graphics/Shader.h>
#include <Simplex/Graphics/GraphicsApiAdapter.h>
#include "GraphicsApiAdapteeMock.h"

using namespace Simplex::Graphics;

TEST ( SimplexGraphicsShader, InheritsFromAsset )
{
  Shader shader = Shader ();
  
  ASSERT_TRUE( static_cast< Simplex::Core::Asset<ShaderDefinition>* >( &shader ) );
}


TEST ( SimplexGraphicsShader, CanSetShaderType )
{
  Shader shader;
  
  shader.SetType ( ShaderType::VERTEX );
  
	ASSERT_EQ ( ShaderType::VERTEX, shader.GetType () );
}

TEST ( SimplexGraphicsShader, CanSetFilename )
{
  Shader shader;
  
  shader.SetFilename ( "blah.vert" );
  
  ASSERT_EQ ( "blah.vert", shader.GetFilename () );
}

TEST ( SimplexGraphicsShader, GetTypeReturnsNoneOnCreation )
{
    Shader shader;
    
    ShaderType type = shader.GetType ();

    ASSERT_EQ ( ShaderType::NONE, type );
}

TEST ( SimplexGraphicsShader, SetsDefinitionCorrectly )
{
  Shader shader;
  ShaderDefinition definition = { "blah.vert", "blah.vert", ShaderType::VERTEX };

  shader.SetDefinition ( definition );
  
  ASSERT_EQ ( definition.name, shader.GetDefinition ().name );
  ASSERT_EQ ( definition.filename, shader.GetDefinition ().filename );
  ASSERT_EQ ( definition.type, shader.GetDefinition ().type );
}

TEST ( SimplexGraphicsShader, ParsesDefinitionCorrectly )
{
  Shader shader;
  ShaderDefinition definition = { "blah.vert", "blah.vert", ShaderType::VERTEX };

  shader.SetDefinition ( definition );
  shader.ParseDefinition ();

  ASSERT_EQ ( definition.filename, shader.GetFilename () );
  ASSERT_EQ ( definition.type, shader.GetType() );
}



TEST ( SimplexGraphicsShader, LoadPassesDataToGraphicsApiAdapter )
{
  GraphicsApiAdapter* adapter = GraphicsApiAdapter::Instance ();
  GraphicsApiAdapteeMock* adaptee = new GraphicsApiAdapteeMock();
  adapter->SetAdaptee ( adaptee );

  Shader shader;
  ShaderDefinition definition = { "blah.vert", "blah.vert", ShaderType::VERTEX };
  shader.SetDefinition ( definition );
  shader.ParseDefinition ();

  shader.Load ();

  ASSERT_TRUE ( adaptee->LoadShaderCalled );

}